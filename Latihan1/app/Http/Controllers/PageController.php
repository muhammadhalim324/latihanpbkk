<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    function Home(){
        $angka1 = 2;
        $angka2 = 6;

        $total = $angka1 + $angka2;
        return view('welcome')->with (['total' => $total]);
    }

    function hitung1(){
        $angka = 85;
        return view('hitung')->with(['nilai' => $angka]);
    }

    function profil(){
        return view('profile021190102');
    }

    function news(){
        return view('news021190102');
    }

    function product(){
        return view('product');
    }

    function Travel(){
        return view('Travel');
    }

    function food(){
        return view('food');
    }
}
