@extends('layouts.app')
@section('title','profile')

@section('main')

    <h2>Food</h2>
    <table>
        <tbody>
            <tr>
                <td>Food</td>
                <td>:</td>
                <td>Pempek atau empek-empek adalah makanan yang terbuat dari daging ikan yang digiling lembut yang dicampur tepung kanji atau tepung sagu, serta komposisi beberapa bahan lain seperti telur, bawang putih yang dihaluskan, penyedap rasa, dan garam.[1] Pempek biasanya disajikan dengan kuah cuka yang memiliki rasa asam, manis, dan pedas. Pempek sering disebut sebagai makanan khas Palembang,[2] meskipun hampir semua daerah di Sumatera Selatan, Jambi dan Bengkulu juga memproduksinya.
                Pempek dapat ditemukan dengan mudah di Kota Palembang, beberapa daerah Sumatera Selatan hingga provinsi Bengkulu; ada yang menjualnya di restoran, ada yang di pinggir jalan, dan pula yang dipikul. Pada tahun 1880-an, penjual biasa memikul satu keranjang penuh pempek sambil berjalan kaki berkeliling menjajakan dagangannya.</td>
            </tr>
            </tbody>
            </table>
@endsection