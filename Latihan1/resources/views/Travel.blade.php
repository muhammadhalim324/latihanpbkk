@extends('layouts.app')
@section('title','profile')

@section('main')

    <h2>Travel Japan</h2>
    <table>
        <tbody>
            <tr>
                <td>Tokyo Tower</td>
                <td>:</td>
                <td>Destinasi wisata Jepang ikonik yang bisa Toppers singgahi saat mendaratkan kaki di Tokyo adalah Menara Tokyo. Objek wisata di Jepang yang berketinggian 332.5 meter ini sangat indah dipandang kala malam menjelang dan lampu-lampu menerangi menara ini. Tak hanya itu, Toppers juga bisa naik ke atas menara untuk memandang keindahan ibukota Jepang ini dari ketinggian.</td>
            </tr>
            <tr>
                <td>Tokyo Disneyland & Disneysea</td>
                <td>:</td>
                <td>Liburan ke Jepang, objek wisata satu ini kerap masuk dalam daftar wajib, khususnya Toppers penggemar dari Disney. Dengan berbagai wahana rekreasi, Tokyo Disneyland dan DisneySea merupakan salah satu taman hiburan dengan pengunjung terbanyak ke-4 di dunia, lho!</td>
            </tr>
            <tr>
                <td>Imperial Palace & East Garden </td>
                <td>:</td>
                <td>Berkunjung ke Tokyo saat traveling ke Jepang, Toppers juga bisa menyempatkan diri singgah ke Istana Kekaisaran Jepang. Di objek wisata Jepang satu ini Toppers sarat akan kekayaan budaya dari negeri matahari terbit ini.
                    Di kompleks kekaisaran Jepang ini, Toppers bisa menikmati keindahan Imperial Palace East Gardens yang memang dibuka untuk umum. Di taman ini juga terdapat bekas benteng pertahanan pada zaman Edo, yakni Honmaru dan Ninomaru.</td>
            </tr>
            <tr>
                <td>Harajuku</td>
                <td>:</td>
                <td>Jika Toppers liburan ke Jepang dengan tujuan wisata belanja, tentu Harajuku merupakan destinasi yang wajib dikunjungi. Distrik yang berada diantara Shinjuku dan Shibuya ini sudah lama populer sebagai salah satu tujuan wisata belanja fashion dunia.</td>
            </tr>
            <tr>
                <td>Sensoji</td>
                <td>:</td>
                <td>Salah satu daya tarik dari wisata Jepang adalah keberadaan kuil-kuil megah dengan arsitektur yang menawan. Di Tokyo, Toppers bisa menemukan Kuil Sensoji, sebuah kuil Buddha Kuno yang sarat akan nilai sejarah.
                Dibangun sebagai bentuk penghormatan kepada Bodhisattva Kannon (Guan Yin), kuil Sensoji telah populer sebagai
                </td>
            </tr>
            <tr>
                <td>Gunung Fuji</td>
                <td>:</td>
                <td>Gunung Fuji adalah salah satu ikon dari Jepang yang tak boleh Toppers lewatkan keindahannya. Meskipun panorama Gunung Fuji bisa Toppers lihat dari kejauhan di Tokyo, namun untuk menikmati keindahan dari objek wisata Jepang satu ini, Toppers juga bisa mendakinya.
                Mendaki gunung Fuji, pada ketinggian 1000 meter Toppers bisa menemukan lima buah danau yang panoramanya tak kalah indah. Selain danau, terdapat juga rest area dimana Toppers bisa menemukan camping ground, museum, tempat pemancingan, hingga onsen atau tempat pemandian air hangat alami.</td>
            </tr>
            </tbody>
            </table>
@endsection