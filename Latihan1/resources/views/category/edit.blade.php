@extends('layouts.app')
@section('title','Halaman Category')
@section('main')

<div class="container">
    <div class="row mt-3 mb-3">
   <form action="{{ url('/category/update') }}" method="post" enctype="multipart/form-data">
       @csrf
       <input type="hidden" name="id" value="{{ $category->id }}">
       <div class="mb-3">
           <label>Kategori</label>
           <select type="text" class="form-control @if($errors->first('name')) is-invalid @endif" name="category_id">
               @foreach ($category as $c)
                <option value="{{ $c->id }}"
                @if($food->category_id == $c->id)
                selected
                @endif>{{ $c->name }}</option>
                @endforeach
           <span class="error invalid-feedback">{{ $errors->first('category_id') }}</span>
    </div>
       <div class="mb-3">
           <label>nama</label>
           <input type="text" class="form-control" name="name" value="{{ $category->name }}">
    </div>
        <div class="mb-3">
            <button class="btn btn-primary">Submit</button>
    </div>
</div>

@endsection