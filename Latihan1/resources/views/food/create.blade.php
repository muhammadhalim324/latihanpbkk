@extends('layouts.app')
@section('title','Halaman Food')
@section('main')

<div class="container">
    <div class="row mt-3 mb-3">
   <form action="{{ url('/food/create') }}" method="post" enctype="multipart/form-data">
       @csrf
       <div class="mb-3">
           <label>Kategori</label>
           <select type="text" class="form-control @if($errors->first('category_id')) is-invalid @endif" name="category_id">
               @foreach ($category as $c)
               <option value="{{ $c->id }}">{{ $c->name }} </option>
               @endforeach
</select>
           <span class="error invalid-feedback">{{ $errors->first('name') }}</span>
    </div>
       <div class="mb-3">
           <label>nama</label>
           <input type="text" class="form-control @if($errors->first('name')) is-invalid @endif" name="name">
           <span class="error invalid-feedback">{{ $errors->first('name') }}</span>
    </div>
    <div class="mb-3">
           <label>harga</label>
           <input type="number" class="form-control @if($errors->first('price')) is-invalid @endif" name="price">
           <span class="error invalid-feedback">{{ $errors->first('price') }}</span>
    </div>
    <div class="mb-3">
        <label>Foto></label>
        <input type="file" class="form-control @if($errors->first('foto'))
        is -invalid @endif" name="foto">
        <span class="error invalid-feedback">({ $errors->first('foto') })</span>
    <div class="mb-3">
           <label>Deskripsi</label>
           <textarea name="description" class="form-control"></textarea>
        </div>
        <div class="mb-3">
            <button class="btn btn-primary">Submit</button>
    </div>
</div>

@endsection