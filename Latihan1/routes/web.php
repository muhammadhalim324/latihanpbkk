<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PageController;
use App\Http\Controllers\newscontroller;
use App\Http\Controllers\FoodController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\TravelController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/biodata', function () {
    return view('biodata');
});

Route::get('/', [PageController::class,'Home']);

Route::get('/profile', [PageController::class,'profil']);

Route::get('/product', [PageController::class,'product']);

Route::get('/Travel', [PageController::class,'Travel']);


Route::get('/hitung',[PageController::class,'hitung1']);

Route::group(['prefix' => '/news'], function () {
    Route::get('/', [newscontroller::class, 'index']);
    Route::get('/add', [newsController::class, 'create']);
    Route::post('/create', [newsController::class, 'save']);
    Route::get('/delete/{id}', [newsController::class, 'delete']);
    Route::get('/edit/{id}', [newsController::class, 'edit']);
    Route::post('/update', [newsController::class, 'update']);
    route::get('/detail/{id}',[newsController::class, 'detail']);
});

Route::group(['prefix' => '/food'], function () {
    Route::get('/', [FoodController::class, 'index']);
    Route::get('/add', [FoodController::class, 'create']);
    Route::post('/create', [FoodController::class, 'save']);
    Route::get('/delete/{id}', [FoodController::class, 'delete']);
    Route::get('/edit/{id}', [FoodController::class, 'edit']);
    Route::post('/update', [FoodController::class, 'update']);
});

Route::group(['prefix' => '/category'], function () {
    Route::get('/', [CategoryController::class, 'index']);
    Route::get('/add', [CategoryController::class, 'create']);
    Route::post('/create', [CategoryController::class, 'save']);
    Route::get('/delete/{id}', [CategoryController::class, 'delete']);
    Route::get('/edit/{id}', [CategoryController::class, 'edit']);
    Route::post('/update', [CategoryController::class, 'update']);
});

Route::group(['prefix' => '/Travel'], function () {
    Route::get('/', [TravelController::class, 'index']);
    Route::get('/add', [TravelController::class, 'create']);
});
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
