-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 17, 2022 at 11:36 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pbkk`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `food`
--

CREATE TABLE `food` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `food`
--

INSERT INTO `food` (`id`, `name`, `price`, `image`, `description`, `created_at`, `updated_at`) VALUES
(1, 'bakso', 15000, NULL, 'bakso bulet', '2022-06-17 00:02:58', '2022-06-17 00:02:58'),
(2, 'mie ayam', 18000, NULL, 'mie ayam spesial', '2022-06-17 00:07:43', '2022-06-17 00:07:43'),
(3, 'kwetiau goreng', 13000, NULL, 'kwetiau goreng enak', '2022-06-17 00:09:06', '2022-06-17 00:09:06'),
(4, 'gorengan', 1000, NULL, 'gorengan pinggir jalan', '2022-06-17 00:09:35', '2022-06-17 00:09:35'),
(5, 'pempek', 2000, NULL, 'pempek sepeda', '2022-06-17 00:09:46', '2022-06-17 00:09:46');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2022_04_22_064123_create_table_news', 1),
(6, '2022_05_27_065341_create_table_food', 1),
(7, '2022_05_27_065453_create_table_travel', 1);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `views` int(11) NOT NULL,
  `thumbnail_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tags` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `description`, `views`, `thumbnail_image`, `tags`, `created_at`, `updated_at`) VALUES
(1, 'one piece', 'spoiler 1023', 0, NULL, NULL, '2022-06-17 01:03:44', '2022-06-17 01:03:44'),
(2, 'One Piece 1053', 'Jakarta - Tinggal satu bab lagi sebelum manga One Piece hiatus selama sebulan. Jelang hiatus, manga One Piece chapter 1053 viral dan tengah dibicarakan di jagat maya Indonesia bahkan berada di posisi pertama terpopuler di Twitter.\r\nSejak kemarin, One Piece 1053 heboh dibahas oleh pencinta manga karangan Eiichiro Oda tersebut. Berbagai spoiler mengenai kelanjutan cerita di Wano Country itu membuat para pembaca penasaran.\r\n\r\nBerbagai komentar bertebaran tentang manga One Piece 1053.\r\n\r\n\r\n\r\n\r\n00:00 / 00:00\r\n\r\n\"Habis baca spoiler #ONEPIECE1053 jadi paham kenapa sakazuki ngomel sama kelakuan beliau ini, iya sih Admiral kelakuan emang preman,\" cuit @NightyONyx.\r\n\r\nBaca artikel detikhot, \"Jelang Hiatus, One Piece 1053 Viral di Jagat Maya Indonesia\" selengkapnya https://hot.detik.com/book/d-6130345/jelang-hiatus-one-piece-1053-viral-di-jagat-maya-indonesia.\r\n\r\n\"Sooo. Buggy D.Clown?! #ONEPIECE1053,\" balas lainnya.\r\n\r\n\"Tidak ada apa-apa. Hanya bentrokan antar yonkou saja. #ONEPIECE1053 #ONEPIECE #buggy,\" kata @ofdepirates.\r\n\r\n\"Ngeri kekuatan admiral satu ini #ONEPIECE1053,\" kata @Bg_aha58.\r\n\r\nSebelumnya, di manga One Piece 152 yang terbit akhir pekan lalu, sejak kejatuhan Kaido yang membuat Monkey D Luffy menang, Agen Pemerintah Dunia membicarakan mengenai hasil akhir pertarungan panjang tersebut.\r\n\r\nBaca artikel detikhot, \"Jelang Hiatus, One Piece 1053 Viral di Jagat Maya Indonesia\" selengkapnya https://hot.detik.com/book/d-6130345/jelang-hiatus-one-piece-1053-viral-di-jagat-maya-indonesia.\r\n\r\nUsai kemenangan mereka, pencinta manga One Piece membicarakan mengenai perayaan Festival Bunga yang dibicarakan selama 7 hari berturut-turut.\r\n\r\nZoro dan Luffy bangun dalam kondisi yang sudah lebih pulih. Kozuki Momonosuke senang dengan bangkitnya mereka berdua.\r\n\r\n\"Aku sudah menunggu kalian berdua sadar, supaya aku bisa memanggil semua orang di negeri ini untuk pesta besar. Ayo lakukan hari ini!\" ucap Kozuki Momonosuke.\r\n\r\nLuffy dan Zoro terkejut melihat sosok pria yang bicara di depannya dan heran dengan penampakan bocah kecil nan tengik yang mereka kenal sebelumnya. Kozuki Momonosuke pun menyiapkan pesta makan dan sake besar-besaran.\r\n\r\nBaca artikel detikhot, \"Jelang Hiatus, One Piece 1053 Viral di Jagat Maya Indonesia\" selengkapnya https://hot.detik.com/book/d-6130345/jelang-hiatus-one-piece-1053-viral-di-jagat-maya-indonesia.\r\n\r\nMereka serta rakyat jelata Wano Country turut merayakan pesta besar tersebut. Kru Topi Jerami juga mandi air hangat di lokasi pemandian umum, masing-masing pria dan perempuan.\r\n\r\nSejarah di Wano pun berubah, ketika anak-anak belajar tentang sejarah, gurunya mengatakan, \"Setelah 20 tahun dalam penindasan sejarah di negeri Wano ini telah berubah. Kita belajar tentang dahsyatnya Kozuki Ode.\"\r\n\r\nBagaimana kelanjutan manga One Piece 1053? Manga One Piece 1053 bakal terbit pada Minggu (19/6/2022) pukul 22.00 WIB di platform legal MANGAPlus Shueisha. Subtitle bahasa Indonesia juga tersedia di MANGAPlus Shueisha.\r\n\r\n\r\n\r\nBaca artikel detikhot, \"Jelang Hiatus, One Piece 1053 Viral di Jagat Maya Indonesia\" selengkapnya https://hot.detik.com/book/d-6130345/jelang-hiatus-one-piece-1053-viral-di-jagat-maya-indonesia.', 0, NULL, NULL, '2022-06-17 01:18:12', '2022-06-17 02:34:24');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `travel`
--

CREATE TABLE `travel` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `food`
--
ALTER TABLE `food`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `travel`
--
ALTER TABLE `travel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `food`
--
ALTER TABLE `food`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `travel`
--
ALTER TABLE `travel`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
